# -*- coding: utf-8 -*-
############
## Author : DJoke
## Date : 2018/12/01
## Usage : Standardize the range of 3 Anchors to tag.
############
import RPi.GPIO as GPIO
import DW1000RangingAnchor as Anchor
import threading
import json
from led import *
GPIO.setwarnings(False)

timer = 10
filename = 'standard'
LED_setup()
LED_hide()
Anchor.AnchorSetup()


def clock():
    global timer
    from time import sleep
    sleep(timer)
    timer = 0
    return
def printResult(fname):
    f = open(fname,'r')
    data = f.read()
    f.close()
    row = data.split('\n')
    n = [0,0,0]
    Sum = [0,0,0]
    for i in row:
        try:
            RangeDict = json.loads(i)
            l = ['0','1','2']
            for key in l:
                if(RangeDict[key]>0):
                    index = l.index(key)
                    n[index] += 1
                    Sum[index] += RangeDict[key]
        except Exception as e:
            #print e
            continue
    if(n[1] and n[2]):
        aver = Sum[0]/n[0] , Sum[1]/n[1] , Sum[2]/n[2]
        tmp = (aver[1]+aver[2])/2
        offset = [round(tmp-aver[0]-0.35,3),round(tmp-aver[1],3),round(tmp-aver[2],3)]
        print 'aver:',round(aver[0],3),round(aver[1],3),round(aver[2],3)
        print 'After standard:',(aver[0]+offset[0],aver[1]+offset[1],aver[2]+offset[2])
        return offset
    return []

thr_clock = threading.Thread(target = clock)
thr_clock.start()
f = open(filename,'w')
while(True):
    try:
        if(not timer):
            break
        RangeDict = Anchor.loop(standardize=True)
        if(not RangeDict):
            continue
        r0 = RangeDict[0]
        r1 = RangeDict[1]
        r2 = RangeDict[2]
        if(r0<0 or r1<0 or r2<0):
            continue
        print RangeDict
        f.write(json.dumps(RangeDict)+'\n')
    except Exception as e:
        print 'Exception:',e
        break
f.close()
LED_hide()
Anchor.AnchorClose()
thr_clock.join()
offset = printResult(filename)
print 'offset:',offset
f = open(filename,'w')
f.write(json.dumps(offset))
f.close()