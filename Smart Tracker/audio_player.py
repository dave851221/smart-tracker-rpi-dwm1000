# -*- coding: utf-8 -*-
############
## Author : DJoke
## Date : 2018/12/12
## Usage : Audio Player.
############
import pygame
# RPi : using "alsamixer" command to set the volume.

class audio_player(object):
    def __init__(self , file_name):
        pygame.mixer.init()
        self.file_name = file_name
        
    def speaking(self):
        return pygame.mixer.music.get_busy()
        
    def speak(self):
        pygame.mixer.music.load(self.file_name)
        pygame.mixer.music.set_volume(1)
        pygame.mixer.music.play()   #non-block
        
    def pause(self):
        pygame.mixer.music.pause()