# -*- coding: utf-8 -*-
############
## Author : DJoke
## Date : 2018/12/01
## Usage : LED API
############
import RPi.GPIO as GPIO
GPIO.setwarnings(False)
PIN_LED = [17,27,22]

def LED_setup():
    GPIO.setmode(GPIO.BCM)
    for pin in PIN_LED:
        GPIO.setup(pin,GPIO.OUT)

def LED_on(index):
    GPIO.setmode(GPIO.BCM)
    GPIO.output(PIN_LED[index], True)
    print 'LED_on:',PIN_LED[index]

def LED_off(index):
    GPIO.setmode(GPIO.BCM)
    GPIO.output(PIN_LED[index], False)

def LED_show(index):
    GPIO.setmode(GPIO.BCM)
    for i in range(len(PIN_LED)):
        if(i == index):
            GPIO.output(PIN_LED[i], True)
        else:
            GPIO.output(PIN_LED[i], False)
    return

def LED_hide():
    GPIO.setmode(GPIO.BCM)
    for i in range(len(PIN_LED)):
        GPIO.output(PIN_LED[i], False)
    return