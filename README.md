# Smart-tracker-rpi-dwm1000

# Overview

# Hardware
* ### 【Raspberry Pi】：

    |   GPIO     | Pin  |        |          |    Pin   | GPIO  |
    | ---------- | :--: | :----: | :-----:  | :---:    | :----:| 
    |            |  1   | 3.3v   | 5v       |2         |       |
    |            |  3   |        | 5v       |4         |       |
    |            |  5   |        | GND      |6         |       |
    |            |  7   |        |          |8         |       |
    |            |  9   | GND    |          |10        |       |
    | GPIO17     |  11  | LED1   |          |12        |GPIO18 |
    | GPIO27     |  13  | LED2   | GND      |14        |       |
    | GPIO22     |  15  | LED3   |          |16        |GPIO23 |
    |            |  17  | 3.3v   |          |18        |GPIO24 |
    |            |  19  | MOSI   | GND      |20        |       |
    |            |  21  | MISO   | CSN1     |22        |GPIO25 |
    |            |  23  | CLK    |          |24        |       |
    |            |  25  | GND    | CSN2     |26        |GPIO7  |
    |            |  27  |        |          |28        |       |
    |            |  29  | IN1    | GND      |30        |       |
    |            |  31  | IN2    | IN3      |32        |       |
    |            |  33  | IN4    | GND      |34        |       |
    |GPIO19      |  35  |        | CSN3     |36        |GPIO16 |
    |            |  37  | ENA    | ENB      |38        |       |
    |            |  39  | GND    |          |40        |       |

* ### 【L298N — Raspberry Pi】：

    |       | #PIN   | 
    | ----- | :-----:| 
    | ENA   | 37     | 
    | IN1   | 29     | 
    | IN2   | 31     | 
    | IN3   | 32     | 
    | IN4   | 33     | 
    | ENB   | 38     | 
    | GND   | 34     | 

* ### 【Raspberry Pi — DWM1000】：
    * RPI：

        |        |    device 0  |   device 1 |    device 2  |        |        |
        | ------ | :------:     | :------:   | :-------:    | :----: | :----: | 
        | IRQ    |35(GPIO 19)   |16(GPIO 23) |18(GPIO 24)   | EXTON  |        |
        | GND    | 25           | same       | same         | WAKEUP |        |
        | CLK    | 23           | same       | same         | RSTN   |        |
        | MISO   | 21           | same       | same         | SYNC   |        |
        | MOSI   | 19           | same       | same         | VCC    | 3.3v   |
        | CSN    |36(GPIO 16)   |26(GPIO 7)  |24(GPIO 8)    | GND    | GND    |
        * CSN : 依照不同chip select做選擇，程式可修改
        * VCC : 電源供應 2.8 V to 3.6 V (3.3v即可)

# Prerequisites

* ### Install pi-spidev
    
        sudo git clone https://github.com/doceme/py-spidev
	    cd py-spidev
    	sudo python setup.py install

    * ### Error log : python.h no such file
        
            sudo apt-get install python-dev


* ### Install monotonic(for python 2):

        git clone https://github.com/atdt/monotonic
        cd monotonic
        sudo python setup.py install

* ### Setup bluetooth server:
        
        sudo apt-get install bluetooth
        sudo apt-get install blueman
        sudo apt-get install python-bluez
        sudo apt-get install python-bluetooth

* ### Enable SPI interface

        sudo raspi-config
    > Interfacing Options >> SPI >> Yes

* ### Bluetooth pairing

        sudo bluetoothctl
    ###
    	[bluetooth]# power on
    	[bluetooth]# agent on
    	[bluetooth]# discoverable on
    	[bluetooth]# pairable on
    	[bluetooth]# scan on

    * ### After the "scan on" command , you will see your device(smart phone) in the list , the copy the MAC address of the device and pair it by using command:
            
            [bluetooth]# pair <address of your device>





